import os
try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup



setup(
    name='SarmanPisi',
    version='0.3.0',
    author='Blue DeviL',
    author_email='bluedevil@sctzine.com',
    url='http://gitlab.com/bluedevil/sarmanpisi/',
    license='LICENSE',
    description='New version checker for pisi packages.',
    long_description=open('README.md').read(),
    classifiers = ['Environment :: Console',
                   'License :: OSI Approved :: MIT',
                   'Programming Language :: Python :: 2',
                   'Programming Language :: Python :: 2.7'],
    keywords = "sarmanpisi new version checker",
    install_requires=["packaging",
                      "colorama",
                      "requests"],
    packages=["sarmanpisi","sarmanpisi.repos"],
    package_data = {"sarmanpisi" : ["repos/*.py", "*.json"]},
    scripts = [os.path.join('scripts', 'sarmanpisi')],
)