ChangeLog
=========

0.3.0 - Blue DeviL - (2020-09-22)
---------------------------------

* FIXED: various typos
* FIXED: docstrings; now compatible with NumPy/SciPy Docstrings
* ADDED: New functionality: version depthness
* ADDED: Legacy Version to Semantic Version converter
* FIXED: More stylish SarmanPisi Banner

0.2.0 - Blue DeviL - (2020-03-27)
---------------------------------

* Launchpad repo added. We can now search through launchpad[dot]net and retrieve version and download url.

0.1.0 - Blue DeviL - (2020-03-19)
---------------------------------

* Initial release
* There are bugs, i am still working on them.

0.0.6 - Blue DeviL - (2020-03-18)
---------------------------------

* Added functionality to parse sourceforge

0.0.5 - Blue DeviL - (2020-03-16)
---------------------------------

* Added functionality to parse gitlab.x.y

0.0.4 - Blue DeviL - (2020-03-14)
---------------------------------

* Added commandline options, using argparser
- help, version and all arguments added
* setup file added
* added script under/usr/bin/sarmanpisi

0.0.3 - Blue DeviL - (2020-03-06)
---------------------------------

* ArtWork done!

0.0.2 - Blue DeviL - (2020-03-04)
---------------------------------

* Added download urls from github and gitlab

0.0.1 - Blue DeviL - (2020-02-23)
---------------------------------

* Added ignore list
* Added a function which parses versions like 2_4_5