![SarmanPisi for PisiLinux](https://gitlab.com/bluedevil/sarmanpisi/-/raw/master/artwork/SarmanPisiBanner.png)

# SarmanPisi - Sürüm Arayan Pisi

SarmanPisi is a commandline tool for searching new versions for Pisi Linux packages. Now it can search, cpan, archlinux/packages, aur, bitbucket, github, gitlab, pypi, sourceforge, launchpad and repology databases.

## Requirements

Runtime requirements are:
```
pisi
colorama
packaging
requests
```
No install/compile time requirements needed

## Usage

If you are using Pisi version 2.x, you should use python 2.7
```
$ sarmanpisi <path_of_pisi_package_sources>
```
Example usage of printing only new versions:
```
$ sarmanpisi my_folder/main/programming
```
Example usage of printing all packages:
```
$ sarmanpisi my_folder/core/ --all
```
By default SarmanPisi shows only new packages and compare major.minor.micro versions. But you can use --depth parameter to say SarmanPisi to check only major or minor changes:<br />
Print all and check minor changes(excludes patch/minor changes)
```
$ sarmanpisi main/ --depth mm -a
```
Print all and compare only major changes
```
$ sarmanpisi main/ --d m --all
```


## Installation

Clone the repo and run setup.py file with `install` argument. That's all.
```
$ git clone https://gitlab.com/bluedevil/sarmanpisi.git
$ python2.7 setup.py install
```

## Deployment

You need Pisi Linux; or you need Pisi Linux docker image to run this tool.

## Authors

* **Blue DeviL** - *Reverser* - [bluedevil](http://gitlab.com/bluedevil)

**HomePage** - [SCTZine](http://www.sctzine.com)

## License

This project is under the MIT License.