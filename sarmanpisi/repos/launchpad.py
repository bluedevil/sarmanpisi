"""
 _________________________________________________________________________________
|                            ,.'   __.'.__  .                                     |
|                            /b`  '-.   .-'  d\                                   |
|                           .=h     /.'.\    =B                                   |
|                           (==h    '   '    =P                                   |
|  _______ __               ?===,          .==P  ______               __ ___      |
| |   _   |  .--.--.-----    {=`==oo____oo==`=} |   _  \ .-----.--.--|__|   |     |
| |.  1   |  |  |  |  -__|    '"o88888888888,;  |.  |   \|  -__|  |  |  |.  |     |
| |.  _   |__|_____|_____|     `?88P^\,?88^\P   |.  |    |_____|\___/|__|.  |___  |
| |:  1    \                    88?\__d88\_/'   |:  1    / .            |:  1   | |
| |::.. .  /                    `8o8888/\88P    |::.. . /               |::.. . | |
| `-------'                      '?oo88oo8P     `------'                `-------' |
|                            ===~88~|\\\\|~====                                   |
| oldurmeyen her darbe, guce guc katar                       gitlab.com/bluedevil |
|_________________________________________________________________________________|
 SarmanPisi - Launchpad Search Module
 _________________________________________________________________________________
 @author   : Blue DeviL <bluedevil@sctzine.com>
 @tester   : ErrorInside <errorinside@sctzine.com>
 @IDE      : Eclipse with PyDev <https://www.pydev.org/>
 @template : Blue DeviL
 @date     : 27/03/2020
 @license  : MIT
 _________________________________________________________________________________
"""
import requests
import json
import re

def getVerFromLaunchpad(packageName):
    """Returns version info and source package download url from launchpad.net

    Parameters
    ----------
    packageName : str
        the name(source package name) where our package sources resides

    Returns
    -------
    str
        version and download url as string
    """
    url = "https://launchpad.net/" + packageName
    searchStr1 = 'Latest version is ?([^\'" >]+)\n\s\s<\/div>\n\n\s\s<ul>\n\s\s\s\s<li>\n\s\s\s\s\s\s<a href=[\'"]?([^\'" >]+)'
    pageSrc = requests.get(url).text
    ver = "0"
    downloadURL = ""
    try:
        output = re.search(searchStr1, pageSrc)

        ver = str(output.group(1).strip())
        downloadURL = str(output.group(2).strip())
        if (ver[0]=="v"):
            ver == ver[1:]
    except AttributeError:
        ver = "0"
        print("[ - ] AttributeError: " + packageName)
    except:
        print("[ - ] Error occured during getting version from github package: %s" % packageName)
    
    #print downloadURL
    return ver, downloadURL

"""
#
# spaghetti Notes For Further Workout
#

# I couldn't find launchpad api, search again

# Especially themes stored in ppa or launchpad have some shitty versioning. Can we fix?

"""