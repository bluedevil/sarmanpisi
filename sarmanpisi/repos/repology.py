"""
 _________________________________________________________________________________
|                            ,.'   __.'.__  .                                     |
|                            /b`  '-.   .-'  d\                                   |
|                           .=h     /.'.\    =B                                   |
|                           (==h    '   '    =P                                   |
|  _______ __               ?===,          .==P  ______               __ ___      |
| |   _   |  .--.--.-----    {=`==oo____oo==`=} |   _  \ .-----.--.--|__|   |     |
| |.  1   |  |  |  |  -__|    '"o88888888888,;  |.  |   \|  -__|  |  |  |.  |     |
| |.  _   |__|_____|_____|     `?88P^\,?88^\P   |.  |    |_____|\___/|__|.  |___  |
| |:  1    \                    88?\__d88\_/'   |:  1    / .            |:  1   | |
| |::.. .  /                    `8o8888/\88P    |::.. . /               |::.. . | |
| `-------'                      '?oo88oo8P     `------'                `-------' |
|                            ===~88~|\\\\|~====                                   |
| oldurmeyen her darbe, guce guc katar                       gitlab.com/bluedevil |
|_________________________________________________________________________________|
 SarmanPisi - Repology Search Module
 _________________________________________________________________________________
 @author   : Blue DeviL <bluedevil@sctzine.com>
 @tester   : ErrorInside <errorinside@sctzine.com>
 @IDE      : Eclipse with PyDev <https://www.pydev.org/>
 @template : Blue DeviL
 @date     : 11/02/2020
 @license  : MIT
 _________________________________________________________________________________
"""
import requests
import json
import re

def getVerFromRepologyAPI(packageName):
    """Retrieves version info from repology : https://repology.org

    Parameters
    ----------
    packageName : str
        package name to be added to url

    Returns
    -------
    str
        string version value(e.g. 2.3.1)
    """
    # sample url = https://repology.org/api/v1/project/python:zeroconf
    # sample url = https://repology.org/api/v1/project/emacs:color-theme
    # python-PyShelCode must convert to: python:pyshellcode
    # perl-Module-Build must convert to perl:module-build

    
    if (packageName[:6]=="python") or (packageName[:4]=="perl") or packageName[:5]=="emacs":
        packageName = packageName.replace("-", ":", 1)

    if("python3" in packageName):
        packageName = packageName.replace("python3", "python")
    elif("py3" in packageName):
        packageName = packageName.replace("py3", "py")
        
    url = "https://repology.org/api/v1/project/" + packageName
    pageSrc = requests.get(url).text
    repologyVersion = "0"
    listSrc = json.loads(pageSrc)

    #print ("len: " + str(len(listSrc)))
    if len(listSrc) == 0:
        repologyVersion = "0"
        return str(repologyVersion)
    else:
        for i in range(len(listSrc)):
            if ((listSrc[i-1]['status']) == "newest") or ((listSrc[i-1]['status']) == "unique"):
                repologyVersion = listSrc[i-1]['version']
        return str(repologyVersion)
    return str(repologyVersion)

"""
#
# spaghetti Notes For Further Workout
#

# TODO:


"""