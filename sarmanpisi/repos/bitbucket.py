"""
 _________________________________________________________________________________
|                            ,.'   __.'.__  .                                     |
|                            /b`  '-.   .-'  d\                                   |
|                           .=h     /.'.\    =B                                   |
|                           (==h    '   '    =P                                   |
|  _______ __               ?===,          .==P  ______               __ ___      |
| |   _   |  .--.--.-----    {=`==oo____oo==`=} |   _  \ .-----.--.--|__|   |     |
| |.  1   |  |  |  |  -__|    '"o88888888888,;  |.  |   \|  -__|  |  |  |.  |     |
| |.  _   |__|_____|_____|     `?88P^\,?88^\P   |.  |    |_____|\___/|__|.  |___  |
| |:  1    \                    88?\__d88\_/'   |:  1    / .            |:  1   | |
| |::.. .  /                    `8o8888/\88P    |::.. . /               |::.. . | |
| `-------'                      '?oo88oo8P     `------'                `-------' |
|                            ===~88~|\\\\|~====                                   |
| oldurmeyen her darbe, guce guc katar                       gitlab.com/bluedevil |
|_________________________________________________________________________________|
 SarmanPisi - BitBucket Search Module
 _________________________________________________________________________________
 @author   : Blue DeviL <bluedevil@sctzine.com>
 @tester   : ErrorInside <errorinside@sctzine.com>
 @IDE      : Eclipse with PyDev <https://www.pydev.org/>
 @template : Blue DeviL
 @date     : 11/02/2020
 @license  : MIT
 _________________________________________________________________________________
"""
import requests
import json
import re

def getVerFromBitBucket(packageDeveloper, packageName):
    """Retrieves version info from bitbucket

    Parameters
    ----------
    packageDeveloper : str
        string package developer or repository name
    packageName : str
        string package name to be added to url

    Returns
    -------
    str
        string version value(e.g. 2.3.1)
    """
    # sample download url:        https://bitbucket.org/cffi/cffi/get/v1.13.2.tar.gz
    # bitbucket page of versions: https://bitbucket.org/cffi/cffi/downloads/?tab=tags
    bitbucketVersion = "0"
    url = "https://bitbucket.org/" + packageDeveloper + "/" +packageName + "/downloads/?tab=tags"
    searchStr = "(?:(\d+\.(?:\d+\.)*\d+)).tar.gz"
    pageSrc = requests.get(url).text
    try:
        bitbucketVersion = re.search(searchStr, pageSrc)
        bitbucketVersion = str(bitbucketVersion.group(1).strip())
    except:
        bitbucketVersion = "0"
    return bitbucketVersion
"""
#
# spaghetti Notes For Further Workout
#

# TODO:
1. Search if we can get version and download path info from bitbucket api??
    '''
    bitbucketVersion = re.findall(searchStr,pageSrc)
    for bbversion in bitbucketVersion:
        try:
            #bitbucketVersion = re.search(searchStr, pageSrc)
            #bitbucketVersion = str(bitbucketVersion.group(0).strip())[1:]
            version.Version(bbversion)
        except version.InvalidVersion:
            print bbversion
            bitbucketVersion.pop(bitbucketVersion.index(bbversion))
        
    return bitbucketVersion[0]
    '''
    #https://api.bitbucket.org/2.0/repositories/mpyne/game-music-emu/refs/tags?sort=-name
    #https://api.bitbucket.org/2.0/repositories/cffi/cffi/refs/tags?sort=-name
"""