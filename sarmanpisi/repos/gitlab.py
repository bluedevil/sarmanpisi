"""
 _________________________________________________________________________________
|                            ,.'   __.'.__  .                                     |
|                            /b`  '-.   .-'  d\                                   |
|                           .=h     /.'.\    =B                                   |
|                           (==h    '   '    =P                                   |
|  _______ __               ?===,          .==P  ______               __ ___      |
| |   _   |  .--.--.-----    {=`==oo____oo==`=} |   _  \ .-----.--.--|__|   |     |
| |.  1   |  |  |  |  -__|    '"o88888888888,;  |.  |   \|  -__|  |  |  |.  |     |
| |.  _   |__|_____|_____|     `?88P^\,?88^\P   |.  |    |_____|\___/|__|.  |___  |
| |:  1    \                    88?\__d88\_/'   |:  1    / .            |:  1   | |
| |::.. .  /                    `8o8888/\88P    |::.. . /               |::.. . | |
| `-------'                      '?oo88oo8P     `------'                `-------' |
|                            ===~88~|\\\\|~====                                   |
| oldurmeyen her darbe, guce guc katar                       gitlab.com/bluedevil |
|_________________________________________________________________________________|
 SarmanPisi - Gitlab Search Module
 _________________________________________________________________________________
 @author   : Blue DeviL <bluedevil@sctzine.com>
 @tester   : ErrorInside <errorinside@sctzine.com>
 @IDE      : Eclipse with PyDev <https://www.pydev.org/>
 @template : Blue DeviL
 @date     : 22/02/2020
 @license  : MIT
 _________________________________________________________________________________
"""
import requests
import json
import re

def getVerFromGitlab(packageDeveloper, packageName, host="gitlab.com"):
    """Retrieves version info from gitlab and also github using hosts:
    e.g.: https://gitlab.freedesktop.org/xdg/pyxdg/
          https://gitlab.com/python-devs/importlib_metadata/

    Parameters
    ----------
    packageDeveloper : str
        user name of the gitlab account
    packageName : str
        the name(repo name or source package name) where our package sources resides

    Returns
    -------
    str
        version and download url as string
    """
    # sample url (for project info) = https://gitlab.com/api/v4/projects/python-devs%2Fimportlib_metadata
    # sample url (for tags/version)= https://gitlab.com/api/v4/projects/python-devs%2Fimportlib_metadata/repository/tags
    # search url:
    urlAPI = "https://" + host + "/api/v4/projects/" + packageDeveloper + "%2F"+ packageName + "/repository/tags"
    pageSrcAPI = requests.get(urlAPI).text
    url = ("https://%s/%s/%s/-/tags" %(host, packageDeveloper,packageName))
    pageSrc = requests.get(url).text
    gitlabVersion = "0"
    downloadURL = ""
    if len(json.loads(pageSrcAPI)) == 0:
        return gitlabVersion
    else:
        gitlabVersion = json.loads(pageSrcAPI)[0]['name']
        #   
        # get source archive url
        #
        try:
            hrefList = re.findall(r'href=[\'"]?([^\'" >]+)', pageSrc)
            foundCondition = False
            for archiveType in [".tar.xz", ".tar.gz", ".tar.bz2", ".zip"]:
                for href in  hrefList:
                    if archiveType in href:
                        downloadURL = ("https://%s%s" % (host, href))
                        foundCondition = True
                        break
                if foundCondition == True:
                    break
        except:
            print("[ - ] Error : getting download url for %s" % url)
        return gitlabVersion, downloadURL
    return gitlabVersion

"""
#
# spaghetti Notes For Further Workout
#

# TODO:


"""