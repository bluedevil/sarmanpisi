"""
 _________________________________________________________________________________
|                            ,.'   __.'.__  .                                     |
|                            /b`  '-.   .-'  d\                                   |
|                           .=h     /.'.\    =B                                   |
|                           (==h    '   '    =P                                   |
|  _______ __               ?===,          .==P  ______               __ ___      |
| |   _   |  .--.--.-----    {=`==oo____oo==`=} |   _  \ .-----.--.--|__|   |     |
| |.  1   |  |  |  |  -__|    '"o88888888888,;  |.  |   \|  -__|  |  |  |.  |     |
| |.  _   |__|_____|_____|     `?88P^\,?88^\P   |.  |    |_____|\___/|__|.  |___  |
| |:  1    \                    88?\__d88\_/'   |:  1    / .            |:  1   | |
| |::.. .  /                    `8o8888/\88P    |::.. . /               |::.. . | |
| `-------'                      '?oo88oo8P     `------'                `-------' |
|                            ===~88~|\\\\|~====                                   |
| oldurmeyen her darbe, guce guc katar                       gitlab.com/bluedevil |
|_________________________________________________________________________________|
 SarmanPisi - Github Search Module
 _________________________________________________________________________________
 @author   : Blue DeviL <bluedevil@sctzine.com>
 @tester   : ErrorInside <errorinside@sctzine.com>
 @IDE      : Eclipse with PyDev <https://www.pydev.org/>
 @template : Blue DeviL
 @date     : 11/02/2020
 @license  : MIT
 _________________________________________________________________________________
"""
import requests
import json
import re
#from pathlib import Path

def getVerFromGithub(packageDeveloper, packageName):
    """Returns version info and source package download url from github.com

    Parameters
    ----------
    packageDeveloper : str
        user name of the github account
    packageName : str
        the name(repo name or source package name) where our package sources resides

    Returns
    -------
    str
        version and download url as string
    """
    url = "https://github.com/" + packageDeveloper + "/" +packageName +"/tags"
    #searchStr = '<a href="/' + packageDeveloper + '/' +packageName +'/releases/tag/(.[^">]*)'
    searchStr1 = '(?:(\d+[\._](?:\d+[\._])*\d+))">'                   # 4.1.1 or  4+2+3
    searchStr2 = '(?:(\d+[\._](?:\d+[\._])*\d+))[A-Za-z0-9\-\_ ]+">'  # release-2.1.11-stable
    searchStr3 = '(20\d{2})(\d{2})(\d{2})'                            # 20200301 => papirus-icon-theme
    pageSrc = requests.get(url).text
    ver = "0"
    downloadURL = ""
    try:
        ver1 = re.search(searchStr1, pageSrc)
        ver2 = re.search(searchStr2, pageSrc)
        ver3 = re.search(searchStr3, pageSrc)
        if ( ver1 != None ):
            ver = ver1
            ver = str(ver.group(1).strip())
        elif ( ver2 != None ):
            ver = ver2
            ver = str(ver.group(1).strip())
        elif ( ver3 != None ):
            ver = ver3
            ver = str(ver.group().strip())
        
        
        
        #ver = ver1 if (ver1 != None) else (ver2)

        #ver = str(ver.group(1).strip())
        if (ver[0]=="v"):
            ver == ver[1:]
    except AttributeError:
        ver = "0"
        print("[ - ] AttributeError: " + packageName)
    except:
        print("[ - ] Error occured during getting version from github package: %s" % packageName)
    
    #   
    # get source archive url
    #
    try:
        hrefList = re.findall(r'href=[\'"]?([^\'" >]+)', pageSrc)
        foundCondition = False
        for archiveType in [".tar.xz", ".tar.gz", ".tar.bz2", ".zip"]:
            for href in  hrefList:
                if archiveType in href:
                    downloadURL = "https://github.com/" + href
                    foundCondition = True
                    # 
                    # TODO:
                    # try to get version from download url
                    #
                    #veri = Path(href) # we can use this with py3
                    #veri = re.findall(r'([^\/v]+)(?=\.\w+$)', href)#2.11.1.tar
                    #print veri
                    #print veri[0][:-4]
                    break
            if foundCondition == True:
                break

    except:
        print("[ - ] Error : getting download url for %s" % url)
    #print downloadURL
    return ver, downloadURL

"""
#
# spaghetti Notes For Further Workout
#

# Below we can see that version number resides as a href value. We retrieve info from there:
sample tag: <a href="/rthalley/dnspython/releases/tag/1.16.0">

# A sample url of a github repo
sample url= https://github.com/googleapis/oauth2client

# Github provides json based api but it has restrictions, maybe later i will handle them
https://api.github.com/repos/warner/python-ecdsa/tags -> info from tags some times there is no info in releases
url = "https://api.github.com/repos/" +packageDeveloper + "/" + packageName +"/releases/latest"

# TODO:
Get version from download urls
main/programming/misc/icu4c/            <a href="/unicode-org/icu/releases/tag/release-66-1">

"""