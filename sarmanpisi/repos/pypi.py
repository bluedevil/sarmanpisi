"""
 _________________________________________________________________________________
|                            ,.'   __.'.__  .                                     |
|                            /b`  '-.   .-'  d\                                   |
|                           .=h     /.'.\    =B                                   |
|                           (==h    '   '    =P                                   |
|  _______ __               ?===,          .==P  ______               __ ___      |
| |   _   |  .--.--.-----    {=`==oo____oo==`=} |   _  \ .-----.--.--|__|   |     |
| |.  1   |  |  |  |  -__|    '"o88888888888,;  |.  |   \|  -__|  |  |  |.  |     |
| |.  _   |__|_____|_____|     `?88P^\,?88^\P   |.  |    |_____|\___/|__|.  |___  |
| |:  1    \                    88?\__d88\_/'   |:  1    / .            |:  1   | |
| |::.. .  /                    `8o8888/\88P    |::.. . /               |::.. . | |
| `-------'                      '?oo88oo8P     `------'                `-------' |
|                            ===~88~|\\\\|~====                                   |
| oldurmeyen her darbe, guce guc katar                       gitlab.com/bluedevil |
|_________________________________________________________________________________|
 SarmanPisi - PyPi Search Module
 _________________________________________________________________________________
 @author   : Blue DeviL <bluedevil@sctzine.com>
 @tester   : ErrorInside <errorinside@sctzine.com>
 @IDE      : Eclipse with PyDev <https://www.pydev.org/>
 @template : Blue DeviL
 @date     : 11/02/2020
 @license  : MIT
 _________________________________________________________________________________
"""
import requests
import json
import re

def getVerFromPyPi(packageName):
    """Returns version info from pypi.org

    Parameters
    ----------
    packageName : str
        string package name to be added to url

    Returns
    -------
    str
        string version value(e.g. 2.3.1)
    """
    # sample pypi.io: https://pypi.io/packages/source/b/beautifulsoup4/beautifulsoup4-4.8.2.tar.gz
    url = "https://pypi.python.org/pypi/"+ packageName + "/json"
    #print url
    response = ""
    pypiVersion = "0"
    try:
        response = requests.get(url).text
        pypiVersion = json.loads(response)['info']['version']
    except:
        pypiVersion = "0"
    return pypiVersion

"""
#
# spaghetti Notes For Further Workout
#

# TODO:


"""