"""
 _________________________________________________________________________________
|                            ,.'   __.'.__  .                                     |
|                            /b`  '-.   .-'  d\                                   |
|                           .=h     /.'.\    =B                                   |
|                           (==h    '   '    =P                                   |
|  _______ __               ?===,          .==P  ______               __ ___      |
| |   _   |  .--.--.-----    {=`==oo____oo==`=} |   _  \ .-----.--.--|__|   |     |
| |.  1   |  |  |  |  -__|    '"o88888888888,;  |.  |   \|  -__|  |  |  |.  |     |
| |.  _   |__|_____|_____|     `?88P^\,?88^\P   |.  |    |_____|\___/|__|.  |___  |
| |:  1    \                    88?\__d88\_/'   |:  1    / .            |:  1   | |
| |::.. .  /                    `8o8888/\88P    |::.. . /               |::.. . | |
| `-------'                      '?oo88oo8P     `------'                `-------' |
|                            ===~88~|\\\\|~====                                   |
| oldurmeyen her darbe, guce guc katar                       gitlab.com/bluedevil |
|_________________________________________________________________________________|
 SarmanPisi - ftp.gnome.org/ftp.acc.umu.se Search Module
 _________________________________________________________________________________
 @author   : Blue DeviL <bluedevil@sctzine.com>
 @tester   : ErrorInside <errorinside@sctzine.com>
 @IDE      : Eclipse with PyDev <https://www.pydev.org/>
 @template : Blue DeviL
 @date     : 17/03/2020
 @license  : MIT
 _________________________________________________________________________________
"""
import requests
import json
import re

def getVerFromGnomeHostAPI(projectName):
    """Retrieves version info from ftp.gnome.org : ftp.acc.umu.se

    Parameters
    ----------
    projectName : str
        project name to be added to url

    Returns
    -------
    str
        string version value(e.g. 2.3.1)
    """
    # sample url = http://ftp.acc.umu.se/pub/GNOME/sources/libxml/cache.json
   
    gnomeHostVersion = "0"
   
    # url = "http://ftp.acc.umu.se/pub/GNOME/sources/" + projectName + "/cache.json"
    # url = "ftp://ftp.gnome.org/pub/GNOME/sources/" + projectName + "/cache.json"
    url = "http://ftp.gnome.org/pub/GNOME/sources/" + projectName + "/cache.json"
    pageSrc = requests.get(url).text
    listSrc = json.loads(pageSrc)
    #print listSrc[2]
    #print ("len: " + str(len(listSrc)))
    if len(listSrc) == 0:
        gnomeHostVersion = "0"
        return str(gnomeHostVersion)
    else:
        verList = listSrc[2][projectName]
        #print len(listSrc[2][projectName])
        gnomeHostVersion = str(verList[-1])
        #print ftpAccUmuSeVersion
    return str(gnomeHostVersion)

"""
#
# spaghetti Notes For Further Workout
#

# TODO:
After pisi3 use pathlib module of python3 to get filename/version

"""