"""
 _________________________________________________________________________________
|                            ,.'   __.'.__  .                                     |
|                            /b`  '-.   .-'  d\                                   |
|                           .=h     /.'.\    =B                                   |
|                           (==h    '   '    =P                                   |
|  _______ __               ?===,          .==P  ______               __ ___      |
| |   _   |  .--.--.-----    {=`==oo____oo==`=} |   _  \ .-----.--.--|__|   |     |
| |.  1   |  |  |  |  -__|    '"o88888888888,;  |.  |   \|  -__|  |  |  |.  |     |
| |.  _   |__|_____|_____|     `?88P^\,?88^\P   |.  |    |_____|\___/|__|.  |___  |
| |:  1    \                    88?\__d88\_/'   |:  1    / .            |:  1   | |
| |::.. .  /                    `8o8888/\88P    |::.. . /               |::.. . | |
| `-------'                      '?oo88oo8P     `------'                `-------' |
|                            ===~88~|\\\\|~====                                   |
| oldurmeyen her darbe, guce guc katar                       gitlab.com/bluedevil |
|_________________________________________________________________________________|
 SarmanPisi - CPAN Search Module
 _________________________________________________________________________________
 @author   : Blue DeviL <bluedevil@sctzine.com>
 @tester   : ErrorInside <errorinside@sctzine.com>
 @IDE      : Eclipse with PyDev <https://www.pydev.org/>
 @template : Blue DeviL
 @date     : 10/02/2020
 @license  : MIT
 _________________________________________________________________________________
"""
import requests
import json
import re

def getVerFromCpanSource(packageName):
    """Returns version number from cpan.org

    Parameters
    ----------
    packageName : str
        string package name to be added to url

    Returns
    -------
    str
        string version value(e.g. 2.3.1)
    """
    url = "https://metacpan.org/release/" + packageName
    pageSrc = requests.get(url).text
    cpanVersion = "0"
    searchStr = "(.[^-]*).tar.gz"
    searchStr2 = "(.[^-]*).tgz"
    try:
        cpanVersion = re.search(searchStr, pageSrc)
        if cpanVersion == None:
            cpanVersion = re.search(searchStr2, pageSrc)
        cpanVersion = str(cpanVersion.group(1).strip())[1:]
    except:
        cpanVersion = "0"
    return cpanVersion

def getVerFromCpanAPI(packageName):
    '''
    returns version number using cpan api
    '''
    url = "https://fastapi.metacpan.org/release/" + packageName
    response = requests.get(url).text
    cpanVersion = "0"
    try:
        cpanVersion = json.loads(response)['version']
    except KeyError:
        print("KeyError; %s" % packageName)
    return str(cpanVersion)