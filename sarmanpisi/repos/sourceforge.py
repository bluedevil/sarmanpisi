"""
 _________________________________________________________________________________
|                            ,.'   __.'.__  .                                     |
|                            /b`  '-.   .-'  d\                                   |
|                           .=h     /.'.\    =B                                   |
|                           (==h    '   '    =P                                   |
|  _______ __               ?===,          .==P  ______               __ ___      |
| |   _   |  .--.--.-----    {=`==oo____oo==`=} |   _  \ .-----.--.--|__|   |     |
| |.  1   |  |  |  |  -__|    '"o88888888888,;  |.  |   \|  -__|  |  |  |.  |     |
| |.  _   |__|_____|_____|     `?88P^\,?88^\P   |.  |    |_____|\___/|__|.  |___  |
| |:  1    \                    88?\__d88\_/'   |:  1    / .            |:  1   | |
| |::.. .  /                    `8o8888/\88P    |::.. . /               |::.. . | |
| `-------'                      '?oo88oo8P     `------'                `-------' |
|                            ===~88~|\\\\|~====                                   |
| oldurmeyen her darbe, guce guc katar                       gitlab.com/bluedevil |
|_________________________________________________________________________________|
 SarmanPisi - Sourceforge Search Module
 _________________________________________________________________________________
 @author   : Blue DeviL <bluedevil@sctzine.com>
 @tester   : ErrorInside <errorinside@sctzine.com>
 @IDE      : Eclipse with PyDev <https://www.pydev.org/>
 @template : Blue DeviL
 @date     : 16/03/2020
 @license  : MIT
 _________________________________________________________________________________
"""
import requests
import json
import re
import os

def getVerFromSourceForgeAPI(projectName):
    """Retrieves version info from sourceforge : https://sourceforge.net

    Parameters
    ----------
    projectName : str
        project name to be added to url

    Returns
    -------
    str
        string version value(e.g. 2.3.1)
    """
    # sample url = https://sourceforge.net/projects/dejavu/best_release.json
    sourceforgeVersion = "0"
    if (projectName == "pisilinux"):
        return sourceforgeVersion 
   
    else:
        try:
            url = "https://sourceforge.net/projects/" + projectName + "/best_release.json"
            pageSrc = requests.get(url).text
            listSrc = json.loads(pageSrc)
        
            #print ("len: " + str(len(listSrc)))
            if len(listSrc) == 0:
                sourceforgeVersion = "0"
                return str(sourceforgeVersion)
            filename = listSrc['release']['filename']
            #sourceforgeVersion = os.path.splitext(os.path.basename(filename))[0]
            ver = re.search(r'(?:(\d+[\._](?:\d+[\._])*\d+))', filename)
            sourceforgeVersion = ver.group(1).strip()
            #print sourceforgeVersion
        except AttributeError:
            ver = "0"
            print("[ - ] AttributeError: " + projectName)
            sourceforgeVersion = "0"
        except:
            print("[ - ] Error occured during getting version from sourceforge package: %s" % projectName)
            sourceforgeVersion = "0"
    #print sourceforgeVersion
    return str(sourceforgeVersion)

"""
#
# spaghetti Notes For Further Workout
#

# TODO:
After pisi3 use pathlib module of python3 to get filename/version

Explodes on -> filename "/disktype/9/disktype-9.tar.gz"
explodes on -> /git/core/system/base/lsb-release/
                solution change download address to https://downloads.sourceforge.net/lsb/lsb-release-1.4.tar.gz
# NOTES:
Some host links that pspec.xml files have:
https://sourceforge.net/projects/ktageditor/files/Source/0.2.0/ktageditor.zip
mirrors://sourceforge/libmms/libmms-0.6.4.tar.gz
mirrors://sourceforge/pidgin/pidgin-2.13.0.tar.bz2
mirrors://sourceforge/openslp/openslp-2.0.0.tar.gz
mirrors://sourceforge/netcat/netcat-0.7.1.tar.bz2
mirrors://sourceforge/project/libquicktime/libquicktime/1.2.4/libquicktime-1.2.4.tar.gz
"""