"""
 _________________________________________________________________________________
|                            ,.'   __.'.__  .                                     |
|                            /b`  '-.   .-'  d\                                   |
|                           .=h     /.'.\    =B                                   |
|                           (==h    '   '    =P                                   |
|  _______ __               ?===,          .==P  ______               __ ___      |
| |   _   |  .--.--.-----    {=`==oo____oo==`=} |   _  \ .-----.--.--|__|   |     |
| |.  1   |  |  |  |  -__|    '"o88888888888,;  |.  |   \|  -__|  |  |  |.  |     |
| |.  _   |__|_____|_____|     `?88P^\,?88^\P   |.  |    |_____|\___/|__|.  |___  |
| |:  1    \                    88?\__d88\_/'   |:  1    / .            |:  1   | |
| |::.. .  /                    `8o8888/\88P    |::.. . /               |::.. . | |
| `-------'                      '?oo88oo8P     `------'                `-------' |
|                            ===~88~|\\\\|~====                                   |
| oldurmeyen her darbe, guce guc katar                       gitlab.com/bluedevil |
|_________________________________________________________________________________|
 SarmanPisi - ArchLinux/Packages Search Module
 _________________________________________________________________________________
 @author   : Blue DeviL <bluedevil@sctzine.com>
 @tester   : ErrorInside <errorinside@sctzine.com>
 @IDE      : Eclipse with PyDev <https://www.pydev.org/>
 @template : Blue DeviL
 @date     : 11/02/2020
 @license  : MIT
 _________________________________________________________________________________
"""
import requests
import json
import re

def getVerFromArchPackages(packageName):
    """Retrieves version info from archlinux packages : https://www.archlinux.org/packages/

    Parameters
    ----------
    packageName : str
        Package name to be added to url

    Returns
    -------
    str
        Version value(e.g. 2.3.1)
    """
    # every package name in archlinux packages database are lowercase
    packageName = packageName.lower()
    # sample url = https://www.archlinux.org/packages/search/json/?name=python-rsa
    # exact search:
    url = "https://www.archlinux.org/packages/search/json/?name=" + packageName
    # search url:
    #url = "https://www.archlinux.org/packages/search/json/?q=" + packageName
    pageSrc = requests.get(url).text
    archPackagesVersion = "0"
    if len(json.loads(pageSrc)['results']) == 0:
        archPackagesVersion = "0"
        return archPackagesVersion
    elif  len(json.loads(pageSrc)['results']) == 1:
        archPackagesVersion = json.loads(pageSrc)['results'][0]['pkgver']
        return archPackagesVersion
    else:
        archPackagesVersion = json.loads(pageSrc)['results'][0]['pkgver']
        return archPackagesVersion
    return archPackagesVersion

"""
#
# spaghetti Notes For Further Workout
#

# TODO:


"""