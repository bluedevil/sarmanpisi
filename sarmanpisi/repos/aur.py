"""
 _________________________________________________________________________________
|                            ,.'   __.'.__  .                                     |
|                            /b`  '-.   .-'  d\                                   |
|                           .=h     /.'.\    =B                                   |
|                           (==h    '   '    =P                                   |
|  _______ __               ?===,          .==P  ______               __ ___      |
| |   _   |  .--.--.-----    {=`==oo____oo==`=} |   _  \ .-----.--.--|__|   |     |
| |.  1   |  |  |  |  -__|    '"o88888888888,;  |.  |   \|  -__|  |  |  |.  |     |
| |.  _   |__|_____|_____|     `?88P^\,?88^\P   |.  |    |_____|\___/|__|.  |___  |
| |:  1    \                    88?\__d88\_/'   |:  1    / .            |:  1   | |
| |::.. .  /                    `8o8888/\88P    |::.. . /               |::.. . | |
| `-------'                      '?oo88oo8P     `------'                `-------' |
|                            ===~88~|\\\\|~====                                   |
| oldurmeyen her darbe, guce guc katar                       gitlab.com/bluedevil |
|_________________________________________________________________________________|
 SarmanPisi - ArchLinux User Repository(AUR) Search Module
 _________________________________________________________________________________
 @author   : Blue DeviL <bluedevil@sctzine.com>
 @tester   : ErrorInside <errorinside@sctzine.com>
 @IDE      : Eclipse with PyDev <https://www.pydev.org/>
 @template : Blue DeviL
 @date     : 11/02/2020
 @license  : MIT
 _________________________________________________________________________________
"""
import requests
import json
import re

def getVerFromAUR(packageName):
    """Retrieves version info from archlinux AUR pages

    Parameters
    ----------
    packageName : str
        Package name to be added to url

    Returns
    -------
    str
        Version value(e.g. 2.3.1)
    """
    url = "https://aur.archlinux.org/rpc/?v=5&type=info&arg[]=" + packageName
    pageSrc = requests.get(url).text
    aurVersion = "0"
    if len(json.loads(pageSrc)['results']) == 0:
        aurVersion = "0"
        return aurVersion
    elif  len(json.loads(pageSrc)['results']) == 1:
        aurVersion = json.loads(pageSrc)['results'][0]['Version']
        return aurVersion
    else:
        aurVersion = json.loads(pageSrc)['results'][0]['Version']
        return aurVersion
    return aurVersion

"""
#
# spaghetti Notes For Further Workout
#

# TODO:


"""