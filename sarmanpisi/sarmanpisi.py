#!/usr/bin/python
"""
            |\__/,|   (`\                                                                                .--------.  
  ____    _.|o o  |_   ) )                                        ____  _     _                          | miyavv | 
 / ___|  (((---((( _ __ ___   __ _ _ __    /\_/\           ___   |  _ \(_)___(_)  _._     _,-'""`-._     .--------. 
 \___ \ / _` | '__| '_ ` _ \ / _` | '_ \  = ^_^ =_______    \ \  | |_) | / __| | (,-.`._,'(       |\`-/|  /         
  ___) | (_| | |  | | | | | | (_| | | | |  __,      __(  \.__) ) |  __/| \__ \ |     `-.-' \ )-`( , o o) /     __QQ 
 |____/ \__,_|_|  |_| |_| |_|\__,_|_| |_| <_____>__(_____)____/  |_|   |_|___/_| __________`-____\`_`"'-_     (_)_">
                                                                                                             _)     
"""

__version__ = "0.3.0"

title = """
[ SarmanPisi v%s...................................................New Pisi Package Version Checker for Pisi Linux ]
""" % __version__

info = """
[ Application...................SarmanPisi ] * [ Author.................Blue DeviL // SCT ]
[ Version............................%s ] * [ E-mail.............bluedevil@sctzine.com ]
[ Tester..................PisiLinux Admins ] * [ Repo................gitlab.com/bluedevil ]
[ PisiLinux Home.....https://pisilinux.org ] * [ Website...........http://www.sctzine.com ]
""" % __version__

import os
import re
import sys      # handle commandline arguments
import json     # parse json object
import requests # get source of an webpage
import colorama # make commandline colorful
import operator # sort tuples inside lists
import argparse # handle/parse commandline arguments

#sys.path.insert(1, 'repos')
#sys.path.append('./repos')
from repos.aur import getVerFromAUR
from repos.pypi import getVerFromPyPi
from repos.cpan import getVerFromCpanAPI
from repos.github import getVerFromGithub
from repos.gitlab import getVerFromGitlab
from repos.gnome import getVerFromGnomeHostAPI
from repos.bitbucket import getVerFromBitBucket
from repos.launchpad import getVerFromLaunchpad
from repos.archpkg import getVerFromArchPackages
from repos.repology import getVerFromRepologyAPI
from repos.sourceforge import getVerFromSourceForgeAPI

from colorama import Fore, Back, Style # make versions colorful
from pisi.specfile import Archive
from pisi.specfile import Packager
from pisi.specfile import SpecFile     # makes pspec files an object
from packaging import version          # compare versions

colorama.init()
__path__ =  os.path.dirname(os.path.abspath(__file__))

def getPspecFiles(path, listPspec):
    """Returns pspec.xml files as a list from given path argument
    Parameters
    ----------
    path : str
        path of a directory including pisi package sources
    listSpec : list
        empty list
    
    Returns
    -------
    listPspec : list
        listPspec is a list full with package directory path and pspex.xml file path.
    """
    for root, dirs, files in os.walk(path):
        for file in files:
            if file.endswith('pspec.xml'):
                listPspec.append((root, os.path.join(root, file)))
    return listPspec

def compareVersions(localVer, onlineVer, depth="mmm"):
    """returns if local version of package is lower/higher than the online one
    While making this comparison, gets the depthness of version from user.
    mmm = check major.minor.micro
    mm  = check major.minor
    m   = check major

    Parameters
    ----------
    localver : str
        local version is got from local pspec.xml file
    onlineVer : str
        online version of the package retrieved from web
    depth : str
        depthness level of version; major, major.micro or major.minor.micro

    Returns
    -------
    bool
        true if onlineVersion is higher
             e.g. pisi_files => [('../../contrib/programming/language/perl/perl-DBD-SQLite', '../../contrib/programming/language/perl/perl-DBD-SQLite/pspec.xml')]
    """

    if (version.parse(onlineVer).__class__.__name__ == "LegacyVersion"):
        #print("[ V ] %s has legacy versioning" % onlineVer)
        onlineVer = convertLegacy2Version(onlineVer)
    
    if (version.parse(localVer).__class__.__name__ == "LegacyVersion"):
        #print("[ V ] localVer has legacy versioning %s" % localVer)
        localVer = convertLegacy2Version(localVer)

    if depth == "mmm":
        if (version.parse(localVer) < version.parse(onlineVer)):
            ''' return True if there is new version'''
            return True
        else:
            return False
        
    elif depth == "mm":
        if (version.parse(str(version.parse(localVer).major) + "." + str(version.parse(localVer).minor)) < version.parse(str(version.parse(onlineVer).major) + "." + str(version.parse(onlineVer).minor))):
            # return True if there is new version
            # version.parse(str(version.parse("localVer").major) + "." + str(version.parse("localVer").minor)) < version.parse(str(version.parse("onlineVer").major) + "." + str(version.parse("onlineVer").minor))
            return True
        else:
            return False
    
    elif depth == "m":
        if (version.parse(localVer).major < version.parse(onlineVer).major):
            # return True if there is new version
            return True
        else:
            return False
        
def convertLegacy2Version(legVersion):
    """Some developers don't know versioning or semantic versioning. They haven't read PEP-440
    Those developers upload sources with some prefixes. I try to remove those prefixes here:

    Parameters
    ----------
    legVersion : str
        legacy version e.g.: 1_3_19

    Returns
    -------
    ver: str
        semantic version e.g: 1.3.19
    """
    ver = legVersion
 
    for leg in ["ver-", "ver", "version-", "version", "release-", "release", "rel-", "rel"]:
        if(legVersion.find(leg) != -1):
            ver = legVersion.replace(leg, "")
            break

    #Also some losers type underscores instead of periods *sigh*
    ver = ver.replace("_", ".")
    
    #Last check if still LegacyVersion then set it zer0
    if (version.parse(ver).__class__.__name__ == "LegacyVersion"):
        #print("[ V ] ver has legacy versioning %s" % ver)
        ver = "0"
    return ver
    
def isIgnoredPackage(packageName, ignoredDict=(__path__+"/ignore_list.json")):
    """
    ignoredDict parameter is a json file which stores ignored, deprecated or
    dropped support packages. This definition uses this dictionary and
    don't let the packages from version control.

    Parameters
    ----------
    packageName : str
        this string is to be checked in the dictionary assigned in ignoredDict
    ignoredDict : dict
        this json file is a dictionary whis stored deprecated package names

    Returns
    -------
    bool
        true if packageName matches one of the keys in ignoredDist
    """
    ignoredJson = open(ignoredDict)
    ignoredPackages=json.load(ignoredJson)
    for ignoredPackage in ignoredPackages:
        if(ignoredPackage == packageName):
            return True
    return False

def controlVersions(arguments,showAll=False, depth="mmm"):
    """control versions of local files and online versions

    Parameters
    ----------
    arguments : str
        path of pspec files; e.g: main/programming or multimedia/sound
    showAll : bool
        enables/disables if sarmanpisi shows packages whcih have new versions or all packages
    depth : str
        depthness level of version; major, major.micro or major.minor.micro

    Returns
    -------
    """
    pisi_files = []
    pisi_files = getPspecFiles(arguments, pisi_files)
    pisi_files.sort(key = operator.itemgetter(0))
    print("Package Path".ljust(80,".") + ":" + "Local Version".ljust(15,".") + ": " + "Online Version".ljust(15,".") + ": " +"Check Host")
    for dir,path in pisi_files:
        # path is xml file path
        spec = SpecFile(path)
        dirName = os.path.basename(os.path.dirname(dir))
        ## Control ignorelist here
        if (isIgnoredPackage(spec.source.name[:])):
            continue
        
        # parse archive objects list and get url, then get domain. so we can check where the source is hosted?
        temp = str(spec.source.archive[0]) # is a list => [URI: https://github.com/tizonia/tizonia-openmax-il/archive/v0.19.0.tar.gz, type: targz, sha1sum: f3ac2e4d9cd089719262f2b9cb4b153dff57081a])
        temp = temp.split(",")[0][5:].split('/')
        host = temp[2]
        checkHost = ""
        packName = ""
        devname = ""
        onlineVersion="0"
        
        # start searching versions according to where their sources are hosted:
        if host == "github.com":
            devName = temp[3]
            packName = temp[4]
            if(dirName == "python" or dirName == "python3"):
                # Some packNames differs from pisi source package names
                # and unfortunately this situation causes to retrieve false versions
                # exception cython3
                if (packName != spec.source.name[:]):
                    packName4Pypi = ""
                    if (dirName == "python"):
                        if(spec.source.name[7:] == "python-"):
                            packName4Pypi = spec.source.name[7:]
                        else:
                            packName4Pypi = packName
                    elif (dirName == "python3"):
                        if ((spec.source.name[:8] == "python3-") and ("python" in packName)):
                            # exception retdec-python
                            packName4Pypi = packName
                        elif(spec.source.name[:8] == "python3-"):
                            packName4Pypi = spec.source.name[8:]
                        else:
                            packName4Pypi = packName
                    pypiVersion = getVerFromPyPi(packName4Pypi)
                    githubVersion, _ = getVerFromGithub(devName,packName)
                    #print("pypiVersion %s" % pypiVersion)
                    #print("githubVersion %s" % githubVersion)
                    onlineVersion, checkHost = (githubVersion, host) if compareVersions(pypiVersion, githubVersion, depth) else (pypiVersion, "pypi.org")
                else:
                    pypiVersion = getVerFromPyPi(packName)
                    githubVersion, _ = getVerFromGithub(devName,packName)
                    onlineVersion, checkHost = (githubVersion, host) if compareVersions(pypiVersion, githubVersion, depth) else (pypiVersion, "pypi.org")
            else:
                githubVersion, _ = getVerFromGithub(devName,packName)
                onlineVersion = githubVersion
                checkHost = host
        elif host == "files.pythonhosted.org" or host == "pypi.io" or host == "pypi.org" or host == "pypi.python.org":
            packName = temp[-1].rsplit('-', 1)[0] #gives package name from pypi host
            #print "host is: " + host + " packName= " + packName
            #now check version
            onlineVersion = getVerFromPyPi(packName)
            checkHost = host
        elif (host == "cpan.metacpan.org") or (host == "search.cpan.org") or (host == "www.cpan.org"):
            packName = temp[-1].rsplit('-', 1)[0]
            onlineVersion = getVerFromCpanAPI(packName)
            checkHost = host
            if onlineVersion == '0':
                onlineVersion = getVerFromArchPackages(spec.source.name[:])
                checkHost = "archlinux.org/packages"
        elif host == "bitbucket.org":
            devName = temp[-4]
            packName = temp[-3]
            if(dirName == "python" or dirName == "python3"):
                pypiVersion = getVerFromPyPi(packName)
                bitbucketVersion = getVerFromBitBucket(devName, packName)
                onlineVersion, checkHost = (bitbucketVersion, host) if compareVersions(pypiVersion, bitbucketVersion, depth) else (pypiVersion, "files.pythonhosted.org")
            else:                
                onlineVersion = getVerFromBitBucket(devName, packName)
                checkHost = host
        elif host == "gitlab.com":
            devName = temp[3]
            packName = temp[4]
            onlineVersion, _ = getVerFromGitlab(devName, packName)
            checkHost = host
        elif host[:6] == "gitlab":#e.g. https://gitlab.freedesktop.org/xdg/pyxdg
            devName = temp[3]
            packName = temp[4]
            onlineVersion, _ = getVerFromGitlab(devName, packName, host)
            checkHost = host
        elif host == "sourceforge" or host == "sourceforge.net":
            if (temp[0] == "mirrors:" and temp[3] == "project"):
                projectName = temp[4]
            else:
                projectName = temp[3] if (host == "sourceforge") else temp[4]
            onlineVersion = getVerFromSourceForgeAPI(projectName)
            checkHost = host
        elif host == "ftp.acc.umu.se" or host == "ftp.gnome.org" or host == "gnome":
            projectName = temp[6] if (host != "gnome") else temp[3]
            onlineVersion = getVerFromGnomeHostAPI(projectName)
            checkHost = host
        elif host == "launchpad.net":
            projectName = temp[3]
            onlineVersion, _ = getVerFromLaunchpad(projectName)
            checkHost = host
        #
        # If online version is 0, let's search repology and aur/archlinux packages
        #
        if (onlineVersion == "0"):#some packages are self hosted
            # before query make python3 packages start with only python
            #print spec.source.name[:]
            selfPackageName = (spec.source.name[:].replace("python3", "python")) if ("python3" in spec.source.name[:]) else spec.source.name[:]
            onlineArchPkgVersion = getVerFromArchPackages(selfPackageName)
            onlineRepologVersion = getVerFromRepologyAPI(selfPackageName)
            onlineAURVersion = getVerFromAUR(selfPackageName)
            
            # sometimes "python" is a post fix, so maybe we need to make it prefix and search
            if (selfPackageName.endswith("python") and onlineArchPkgVersion == "0" and onlineRepologVersion == "0" and onlineAURVersion == "0"):
                selfPackageName = "python-" + selfPackageName[:-7]
                onlineArchPkgVersion = getVerFromArchPackages(selfPackageName)
                onlineRepologVersion = getVerFromRepologyAPI(selfPackageName)
                onlineAURVersion = getVerFromAUR(selfPackageName)
            if (compareVersions(onlineArchPkgVersion, onlineRepologVersion, depth)):
                if (compareVersions(onlineRepologVersion, onlineAURVersion, depth)):
                    onlineVersion = onlineAURVersion
                    checkHost = "aur"
                onlineVersion = onlineRepologVersion
                checkHost = "repology"
            else:
                onlineVersion = onlineArchPkgVersion
                checkHost = "archlinux.org/packages"
            
            # some packages starts with "lib", and repos store these packages only with source name without a lib prefix
            # 'Yes' if fruit == 'Apple' else 'No'
            # pkgName = 
            if (onlineVersion == "0" and spec.source.name[:3] == "lib"):
                onlineArchPkgVersion = getVerFromArchPackages(spec.source.name[3:])
                onlineRepologVersion = getVerFromRepologyAPI(spec.source.name[3:])
                onlineAURVersion = getVerFromAUR(spec.source.name[3:])
                if (compareVersions(onlineArchPkgVersion, onlineRepologVersion, depth)):
                    if (compareVersions(onlineRepologVersion, onlineAURVersion, depth)):
                        onlineVersion = onlineAURVersion
                        checkHost = "aur"
                    onlineVersion = onlineRepologVersion
                    checkHost = "repology"
                else:
                    onlineVersion = onlineArchPkgVersion
                    checkHost = "archlinux.org/packages"
            
        #now print results:
        #print("Package Path".ljust(80,".") + ":" + "Local Version".ljust(15,".") + ":" + "Online Version".ljust(15,".") + "Check Host")
        if (not showAll):
            if (compareVersions(spec.getSourceVersion(), onlineVersion, depth)):
                print dir.ljust(80,".") + ":" + spec.getSourceVersion().ljust(15,".") + ": "+ Fore.RED + onlineVersion.ljust(15,".") + Style.RESET_ALL + ": "+ checkHost
        else: #print all
            if (compareVersions(spec.getSourceVersion(), onlineVersion, depth)):
                print dir.ljust(80,".") + ":" + spec.getSourceVersion().ljust(15,".") + ": "+ Fore.RED + onlineVersion.ljust(15,".") + Style.RESET_ALL + ": "+ checkHost
            elif (onlineVersion == "0"):
                print dir.ljust(80,".") + ":" + spec.getSourceVersion().ljust(15,".") + ": "+ Fore.YELLOW + onlineVersion.ljust(15,".") + Style.RESET_ALL + ": "+ checkHost
            else:
                print dir.ljust(80,".") + ":" + spec.getSourceVersion().ljust(15,".") + ": "+ onlineVersion.ljust(15,".") + ": "+ checkHost

def main():
    """handles commandline arguments and runs this script.
    """
    usage = '\r{}\nusage: %(prog)s <path_package(s)_dir>\n\n' \
            'e.g. : %(prog)s core\n' \
            '     : %(prog)s main/network\n' \
            '     : %(prog)s contrib/hardware/disk/ddrescue'.format((__doc__ + info).ljust(len('usage:')))
    parser = argparse.ArgumentParser(description="SarmanPisi looks for new versions of pisi packages. This script gets path of archive url from pspec.xml files and starts searching if there is a new version. If not found looks for repology and arch database.", prog="sarmanpisi", usage=usage)
    parser.add_argument("path", help="directory path of pisi packages repo or subdiretory")
    parser.add_argument("-a", "--all", action="store_true", help="print all packages")
    parser.add_argument("-d", "--depth", action="store", dest="depth", default="mmm", help="define what do you want to search:\n\t major, minor or micro versions")
    #parser.add_argument("-n", "--new", action="store_true", help="print only new packages")
    parser.add_argument("-v", "--version", action='version', version="%(prog)s 0.3.0")
    
    args = parser.parse_args()
    print(__doc__ + title)
    #print(args.depth)
    
    #print(args.all)
    controlVersions(args.path, args.all, args.depth)

if __name__ == '__main__':
    sys.exit(main())
